import re


class RegexerString(str):
    """Simplify working with regex.

    Examples:
        ::
            >>> RegexerString("Test01 string sentence02")[r"(?P<num>\\d+)", 'num']
            ['01', '02']
            >>> RegexerString("Test01 string sentence02")[r"(?P<num>\\d+)", 1, 'num']
            '02'
            >>> RegexerString("Test01 string sentence02") / r"\\w\\d{2}"
            ['Tes', ' string sentenc']
            >>> RegexerString("Test01 string sentence02") - r"\\d"
            'Test string sentence'

    Raises:
        Exception: type not supported.
        Exception: no item found by index.
    """

    def __sub__(self, other):
        if isinstance(other, str):
            return RegexerString(re.sub(other, "", str(self)))
        else:
            raise ValueError("Not supported!")

    def replace(self, pattern, replacement):
        return RegexerString(re.sub(pattern, replacement, str(self)))

    def __truediv__(self, other):
        if isinstance(other, str):
            f = filter(lambda item: item, re.split(other, self))
            return list([RegexerString(x) for x in f])
        else:
            raise ValueError("Not supported!")

    def __mul__(self, other):
        if isinstance(other, int):
            s = str(self)
            return RegexerString(s * other)
        else:
            raise ValueError("Not supported!")

    def __getitem__(self, index):
        if isinstance(index, str):
            return [x.groupdict() for x in re.finditer(index, self)]
        r = [x.groupdict() for x in re.finditer(index[0], self)]
        if len(index) == 3:
            if isinstance(index[1], int):
                i, group = index[1], index[2]
            if isinstance(index[1], str):
                i, group = index[2], index[1]
            if len(r) <= i:
                raise ValueError(f"There is no item on index {i}")
            return r[i][group]
        elif len(index) == 2 and len(r) == 1:
            return r[0][index[1]]
        else:
            return [x[index[1]] for x in r]


class RegexerList(list):
    def __getitem__(self, index):
        if isinstance(index, int):
            return list(self)[index]
        if isinstance(index, str):
            return [x[index] for x in self]


class Regexer:
    """Simplify working with regex.

    Examples:
        ::
            >>> Regexer([r"(?P<num>\\d+)")("Test01 string sentence02")['num']
            ['01', '02']
            >>> Regexer([r"(?P<num>\\d+)")("Test01 string sentence02")[1]['num']
            '02'

    Raises:
        Exception: type not supported.
        Exception: no item found by index.
    """

    def __init__(self, r):
        self.r = r

    def __call__(self, s):
        return RegexerList(RegexerString(s)[self.r])
