# regexer
Simplify working with regex.

Examples:
```python
>>> Regexer(r"(?P<num>\d+)")("Test01 string sentence02")['num']
['01', '02']
>>> RegexerString("Test01 string sentence02")[r"(?P<num>\d+)", 'num']
['01', '02']
>>> RegexerString("Test01 string sentence02")[r"(?P<num>\d+)", 1, 'num']
'02'
>>> RegexerString("Test01 string sentence02") / r"\w\d{2}"
['Tes', ' string sentenc']
>>> RegexerString("Test01 string sentence02") - r"\d"
'Test string sentence'
```
