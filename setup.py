from setuptools import setup, find_packages

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name="regexer",
    version="0.0.2",
    description="Simplify work with regex.",
    author="Kurovkyi Vladyslav",
    long_description=long_description,
    long_description_content_type="text/markdown",
    python_requires=">=3.6",
    py_modules=['regexer'],
    tests_require=['pytest'],
    test_suite='test_regexer',
    url="https://gitlab.com/vladku/regexer",
    packages=find_packages(include=['regexer', 'regexer.*']),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
