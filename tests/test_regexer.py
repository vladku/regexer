from pytest import raises
from regexer import Regexer as r
from regexer import RegexerString as s

def test_match():
    assert s("Test01")[r"(?P<num>\d+)", 'num'] == '01'

def test_match_list():
    assert s("Test01 string sentence02")[r"(?P<num>\d+)", 'num'] == ['01', '02']

def test_match_index():
    assert s("Test01 string sentence02")[r"(?P<num>\d+)", 1, 'num'] == '02'
    assert s("Test01 string sentence02")[r"(?P<num>\d+)", 'num', 0] == '01'

def test_match_index_fail():
    with raises(ValueError):
        assert s("Test01")[r"(?P<num>\d+)", 1, 'num']

def test_split():
    result = s("Test01 string sentence02") / r"\w\d{2}"
    assert ["Tes", " string sentenc"] == result
    result = [r / r"s" for r in result]
    assert [["Te"], [" ", "tring ", "entenc"]] == result

def test_split_fail():
    with raises(ValueError):
        result = s("Test01 string sentence02") / 1

def test_remove():
    result = s("Test01 string sentence02") - r"\d"
    assert "Test string sentence" == result
    result = result - r"st"
    assert "Te ring sentence" == result

def test_remove_fail():
    with raises(ValueError):
        result = s("Test01 string sentence02") - 1
        
def test_replase():
    result = s("Test01 string sentence02").replace(r"\d", "7")
    assert "Test77 string sentence77" == result
    assert "Test777 st7ring sentence77" == result.replace(r"(?P<test>st)", r"\g<test>7")

def test_replase_group():
    result = s("Test01 string123 sentence02").replace(r"(?P<test>\D)\d{2}(\s|$)", r"7\g<test>\2") 
    assert "Tes7t string123 sentenc7e" == result

def test_add():
    result = s("Test") + "1"
    assert "Test1" == result
    
def test_multiply():
    result = s("*") * 5
    assert "*****" == result
    assert "" == result - r'\*'

def test_regexer():
    num_regex = r(r"(?P<num>\d+)")
    one = num_regex("Test01 string sentence02")
    assert one == [{'num': '01'}, {'num': '02'}]
    assert one["num"] == ['01', '02']
    assert one[0] == {"num": '01'}
    assert num_regex("Test01 string sentence03")["num"] == ['01', '03']